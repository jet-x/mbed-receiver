#include "mbed.h"
#include <cmath>
#include "nRF24L01P.h"
#include "Archive.hpp"

Serial pc (USBTX, USBRX);
I2C i2c(p28,p27);   // sda,scl
nRF24L01P wireless(p5, p6, p7, p8, p9, p10); // mosi, miso, sck, csn, ce, irq

// template for received data
union rpm1_sensor{
    float rpm_value;
    char bytes[4];
    }r1;
    
union rpm2_sensor{
    float rpm_value;
    char bytes[4];
    }r2;
    
union vibr_sensor{
    float vibr_value;
    char bytes[4];
    }v;

union press1_sensor{
    float press_value;
    char bytes[4];
    }p1;
    
union press2_sensor{
    float press_value;
    char bytes[4];
    }p2;

union press3_sensor{
    float press_value;
    char bytes[4];
    }p3;

union press4_sensor{
    float press_value;
    char bytes[4];
    }p4;

union air_sensor{
    float air_value;
    char bytes[4];
    }a;
union temp1_sensor{
    float temp_value;
    char bytes[4];
    }t1;

union temp2_sensor{
    float temp_value;
    char bytes[4];
    }t2;

union supply{
    float supply_value;
    char bytes[4];
    }s;


// template for data to be sent to GUI
struct SensorData{
    float press1, press2, press3, press4;
    float air;
    float temp1, temp2;
    float rpm1, rpm2;
    float vibr;
    float supply;
}sdata;

float rpm1, rpm2, vibr, temp1, temp2;
float press1, press2, press3, press4, air;


const int DATA_SIZE = 22;
// raw data received wirelessly
char data[DATA_SIZE];

void pcHeader(){
    // Display the (default) setup of the nRF24L01+ chip
    pc.printf( "nRF24L01+ Frequency    : %d MHz\r\n",  wireless.getRfFrequency() );
    pc.printf( "nRF24L01+ Output power : %d dBm\r\n",  wireless.getRfOutputPower() );
    pc.printf( "nRF24L01+ Data Rate    : %d kbps\r\n", wireless.getAirDataRate() );
    pc.printf( "nRF24L01+ TX Address   : 0x%010llX\r\n", wireless.getTxAddress() );
    pc.printf( "nRF24L01+ RX Address   : 0x%010llX\r\n\n", wireless.getRxAddress() );
    }

void pcDataOut(){
    pc.printf("Temp1: %4.1f Celcius\n", sdata.temp1);
    pc.printf("Temp2: %4.1f Celcius\n\n", sdata.temp2);
    pc.printf("RPM 1: %4d rpm\n", (int) sdata.rpm1);
    pc.printf("RPM 2: %4d rpm\n", (int) sdata.rpm2);
    pc.printf("Vibr: %5.1f Hz\n\n", sdata.vibr);
    pc.printf("Pres1: %4.1f kPa\n", sdata.press1);
    pc.printf("Pres2: %4.1f kPa\n", sdata.press2);
    pc.printf("Pres3: %4.1f kPa\n", sdata.press3);
    pc.printf("Pres4: %4.1f kPa\n\n", sdata.press4);
    pc.printf("Air: %6.1f m/s\n\n", sdata.air);
    pc.printf("Supply: %3.2f\n\n\n", sdata.supply);
    }


void synchronizeData(){
       
    r1.bytes[2] = data[0];
    r1.bytes[3] = data[1];
    r2.bytes[2] = data[2];
    r2.bytes[3] = data[3];
    v.bytes[2] = data[4];
    v.bytes[3] = data[5];
    p1.bytes[2] = data[6];
    p1.bytes[3] = data[7];
    p2.bytes[2] = data[8];
    p2.bytes[3] = data[9];
    p3.bytes[2] = data[10];
    p3.bytes[3] = data[11];
    p4.bytes[2] = data[12];
    p4.bytes[3] = data[13];
    a.bytes[2] = data[14];
    a.bytes[3] = data[15];
    t1.bytes[2] = data[16];
    t1.bytes[3] = data[17];
    t2.bytes[2] = data[18];
    t2.bytes[3] = data[19];
    s.bytes[2] = data[20];
    s.bytes[3] = data[21];
    
    sdata.rpm1 = r1.rpm_value;
    sdata.rpm2 = r2.rpm_value;
    sdata.vibr = v.vibr_value;
    sdata.press1 = p1.press_value;
    sdata.press2 = p2.press_value;
    sdata.press3 = p3.press_value;
    sdata.press4 = p4.press_value;
    sdata.air = a.air_value;
    sdata.temp1 = t1.temp_value;
    sdata.temp2 = t2.temp_value;
    sdata.supply = s.supply_value;
    }

int main() {
    
    
    wireless.powerUp();
    
    //pcHeader(); // print info on screen
    
    // wireless setup
    wireless.setTransferSize(DATA_SIZE);
    wireless.setReceiveMode();
    wireless.enable();
    
    while(1) {
        // waiting for data to be received
        if (wireless.readable()){
            
            wireless.read(NRF24L01P_PIPE_P0, data, sizeof data);
            wait(1);
            synchronizeData();
            
            #define GUI
            #ifdef GUI
            char *ptr = (char*)&sdata;
            size_t size = sizeof(sdata);
            // send s to synchronize
            pc.putc('s');
            
            for (unsigned i = 0; i < size; ++i) {
                pc.putc(ptr[i]);
            }
            wait(0.2);
            
            #else
            pcDataOut(); // print data on the screen
            wait(0.2);
            #endif
        }
    }
}
