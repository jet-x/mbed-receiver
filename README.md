## Prerequisites
In order to compile the project, you need:

* [gcc arm compiler] ( https://developer.arm.com/open-source/gnu-toolchain/gnu-rm/downloads )

* [mbed cli](https://github.com/ARMmbed/mbed-cli)


## First time steps
* Download the project using
```mbed import git@gitlab.com:jet-x/mbed-receiver.git```
* cd into the project directory
* Set the default target with
```mbed target LPC1768```
* Set the default toolchain with
```mbed toolchain GCC_ARM```

## To update and compile:
* Update with ```mbed update -l```
* Compile with ```mbed compile```


